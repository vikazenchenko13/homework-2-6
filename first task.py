def gen_reversed():
    rev = [2, 7, 87, 34, 5]
    print(rev)
    rev.reverse()
    for i in rev:
        yield i


for i in gen_reversed():
    print(i)
