my_list = [1, 22, 13, 4, 64, 25, 33, 18, 6, 10]
generator = (x**2 for x in my_list if x % 2 == 0)
print(my_list)
for i in generator:
    print(i)

print()

for x in my_list:
    if x % 2 == 0:
        print(x**2)